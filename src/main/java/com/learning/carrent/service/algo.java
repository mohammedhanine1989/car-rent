package com.learning.carrent.service;

public class algo {

    String MyPhrase = "CeciEtUnePhraseColéJeDoisCompterLesMots";
    int NombreDeMots = 0;

    int MyFunction(String MyPhrase){
        int NombreDeMots = 0;

        final int length = MyPhrase.length();
        boolean newWord = true;
        for (int i = 0; i < length; i++) {
            final char c = MyPhrase.charAt(i);

            if (" .,;/".indexOf(c) >= 0) {
                newWord = true;
            } else if (newWord) {
                newWord = false;
                if (Character.isUpperCase(c)) {
                    NombreDeMots++;
                }
            }
        }
        return NombreDeMots;
    }
    }
