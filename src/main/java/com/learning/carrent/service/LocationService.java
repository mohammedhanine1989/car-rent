package com.learning.carrent.service;

import com.learning.carrent.dto.LocationDTO;
import com.learning.carrent.mapping.LocationMapper;
import com.learning.carrent.model.Client;
import com.learning.carrent.model.Location;
import com.learning.carrent.model.Voiture;
import com.learning.carrent.repository.ClientRepository;
import com.learning.carrent.repository.LocationRepository;
import com.learning.carrent.repository.VoitureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LocationService {

    private LocationRepository locationRepository;
    private VoitureRepository voitureRepository;
    private ClientRepository clientRepository;
    private LocationMapper locationMapper;
    private VoitureService voitureService;

    @Autowired
    public LocationService(LocationRepository locationRepository, VoitureRepository voitureRepository, ClientRepository clientRepository, LocationMapper locationMapper, VoitureService voitureService) {
        this.locationRepository = locationRepository;
        this.voitureRepository = voitureRepository;
        this.clientRepository = clientRepository;
        this.locationMapper = locationMapper;
        this.voitureService = voitureService;
    }

    public List<LocationDTO> getAllLocation() {
        List<Location> location = locationRepository.findAll();
        List<LocationDTO> locationDTOS = location.stream().map(locationMapper::locationTolocationDTO).collect(Collectors.toList());
        return locationDTOS;
    }

    public LocationDTO getLocationById(long id) {
        Optional<Location> location = locationRepository.findById(id);
        return location.isPresent() ? locationMapper.locationTolocationDTO(location.get()) : null;
    }

    public LocationDTO insertLocation(LocationDTO locationDTO) {

        Location location = locationMapper.locationDTOTolocation(locationDTO);
        Client client = clientRepository.findById(locationDTO.getClientId()).get();
        Voiture voiture = voitureRepository.findById(locationDTO.getVoitureId()).get();
        location.setClient(client);
        location.setVoiture(voiture);
        location = locationRepository.save(location);
        return locationMapper.locationTolocationDTO(location);

    }

    public LocationDTO upDateLocation(long id, LocationDTO locationDTO) {

        Optional<Location> location = locationRepository.findById(id);

        Location locationToupdate = null;
        if (location.isPresent()) {
            locationToupdate = locationMapper.locationDTOTolocation(locationDTO);
            locationToupdate.setId(id);
            locationToupdate = locationRepository.save(locationToupdate);
            Client client = clientRepository.findById(locationDTO.getClientId()).get();
            Voiture voiture = voitureRepository.findById(locationDTO.getVoitureId()).get();
            location.get().setClient(client);
            location.get().setVoiture(voiture);

        }
        return locationToupdate != null ? locationMapper.locationTolocationDTO(locationToupdate) : null;
    }

    public void deleteByIp(long id) {
        Optional<Location> location = locationRepository.findById(id);
        if (location.isPresent()) {
            clientRepository.deleteById(id);
        }

    }

    public LocationDTO addlocation(LocationDTO locationDTO) {


        if (voitureService.isVoitureDispoInPeriod(locationDTO.getVoitureId(),locationDTO.getDateDebut(),locationDTO.getDateFin())) {
            Location location = locationMapper.locationDTOTolocation(locationDTO);
            Client client = clientRepository.findById(locationDTO.getClientId()).get();
            Voiture voiture = voitureRepository.findById(locationDTO.getVoitureId()).get();
            location.setClient(client);
            location.setVoiture(voiture);
            location = locationRepository.save(location);
            return locationMapper.locationTolocationDTO(location);
        }
        return null;
    }
}
