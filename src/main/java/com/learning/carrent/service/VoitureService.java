package com.learning.carrent.service;

import com.learning.carrent.dto.VoitureDTO;
import com.learning.carrent.mapping.VoitureMapper;
import com.learning.carrent.model.Location;
import com.learning.carrent.model.Voiture;
import com.learning.carrent.repository.LocationRepository;
import com.learning.carrent.repository.VoitureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VoitureService {

    private VoitureRepository voitureRepository;

    private LocationRepository locationRepository;
    private VoitureMapper voitureMapper;

    @Autowired
    public VoitureService(VoitureRepository voitureRepository, LocationRepository locationRepository, VoitureMapper voitureMapper) {
        this.voitureRepository = voitureRepository;
        this.locationRepository = locationRepository;
        this.voitureMapper = voitureMapper;
    }

    public VoitureDTO insertVoiture(VoitureDTO voitureDTO) {
        return voitureMapper.voitureToVoitureDto(voitureRepository.save(voitureMapper.voitureDtoToVoiture(voitureDTO)));
    }

    public VoitureDTO getVoiture(long id) {
        return voitureMapper.voitureToVoitureDto(voitureRepository.findById(id).orElse(null));
    }

    public List<VoitureDTO> getAllVoitures() {
        return voitureRepository.findAll().stream().map(voitureMapper::voitureToVoitureDto).collect(Collectors.toList());
    }

    public VoitureDTO upDateVoiture(long id, VoitureDTO voitureDTO) {
        Optional<Voiture> voiture = voitureRepository.findById(id);
        Voiture voitureToUpdate = null;
        if (voiture.isPresent()) {
            voitureToUpdate = voitureMapper.voitureDtoToVoiture(voitureDTO);
            voitureToUpdate.setId(id);
            voitureToUpdate = voitureRepository.save(voitureToUpdate);
        }
        return voitureToUpdate != null ? voitureMapper.voitureToVoitureDto(voitureToUpdate) : null;
    }

    public void deleteByIp(long id) {
        Optional<Voiture> voiture = voitureRepository.findById(id);
        if (voiture.isPresent()) {
            voitureRepository.deleteById(id);
        }
    }

    public List<VoitureDTO> getVoituresDisponibles() {

        List<Voiture> voitures = voitureRepository.findAll();
        List<VoitureDTO> voituresDisponibles = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        for (Voiture voiture : voitures) {
            if (isVoitureDisponible(now, voiture)) {
                voituresDisponibles.add(voitureMapper.voitureToVoitureDto(voiture));
            }
        }
        return voituresDisponibles;
    }

    public boolean isVoitureDispoInPeriod(Long voitureId, LocalDateTime start2,LocalDateTime end2){
        boolean output = true;
        List<Location> locations = locationRepository.findAllByVoitureId(voitureId);
        output = locations.stream().noneMatch(loc->overlap(loc.getDateDebut(), loc.getDateFin(), start2,end2));
        return output;
    }
    private boolean overlap(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2,LocalDateTime end2){
        return start1.isBefore(end2) && start2.isBefore(end1);
    }

    public boolean isVoitureDisponible(VoitureDTO voitureDTO) {
        LocalDateTime now = LocalDateTime.now();
        return isVoitureDisponible(now,voitureMapper.voitureDtoToVoiture(voitureDTO));
    }

    private boolean isVoitureDisponible(LocalDateTime date, Voiture voiture) {
        List<Location> locations = locationRepository.findAllByVoitureId(voiture.getId());
        return locations.stream().noneMatch(location -> date.isBefore(location.getDateFin()) && date.isAfter(location.getDateDebut()));
    }
}