package com.learning.carrent.service;

import com.learning.carrent.dto.ClientDTO;
import com.learning.carrent.mapping.ClientMapper;
import com.learning.carrent.model.Client;
import com.learning.carrent.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private ClientRepository clientRepository;
    private ClientMapper clientMapper;

    @Autowired
    public ClientService(ClientRepository clientRepository, ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    public List<ClientDTO> getAllClients() {
        List<Client> clients = clientRepository.findAll();
        List<ClientDTO> clientDTOs = clients.stream().map(clientMapper::clientToClientDTO).collect(Collectors.toList());
        return clientDTOs;
    }

    public ClientDTO getClientById(long id) {
        Optional<Client> client = clientRepository.findById(id);
        return client.isPresent() ? clientMapper.clientToClientDTO(client.get()) : null;
    }

    public ClientDTO insertClient(ClientDTO clientDTO) {
        Client client = clientMapper.clientDTOToClient(clientDTO);
        client = clientRepository.save(client);
        return clientMapper.clientToClientDTO(client);
    }

    public ClientDTO upDateClient(long id, ClientDTO clientDTO) {
        Optional<Client> client = clientRepository.findById(id);
        Client clientToUpdate = null;
        if (client.isPresent()) {
            clientToUpdate = clientMapper.clientDTOToClient(clientDTO);
            clientToUpdate.setId(id);
            clientToUpdate = clientRepository.save(clientToUpdate);
        }

        return clientToUpdate != null ? clientMapper.clientToClientDTO(clientToUpdate) : null;
    }

    public void deleteByIp(long id) {
        Optional<Client> client = clientRepository.findById(id);
        if(client.isPresent()){
            clientRepository.deleteById(id);
        }

    }
}