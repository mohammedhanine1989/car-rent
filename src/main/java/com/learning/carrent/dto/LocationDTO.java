package com.learning.carrent.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LocationDTO {

    private Long id;
    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;

    private long clientId;
    private long voitureId;
}
