package com.learning.carrent.dto;


import lombok.Data;

@Data
public class VoitureDTO {

    private Long id;
    private long prix;
    private String marque;
    private String carburant;

}
