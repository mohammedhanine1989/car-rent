package com.learning.carrent.dto;
import com.learning.carrent.validation.annotation.ValidPhoneNumber;
import lombok.Data;
import javax.validation.constraints.*;

@Data
//@ValidPeriode(message="La voiture n'est pas disponible dans cette perioder")
public class ClientDTO {

    private long id;

    @Min(value = 18, message = "Age should not be less than 18")
    @Max(value = 150, message = "Age should not be greater than 150")
    private long age;

    @NotNull(message = "First Name cannot be null")
    private String nom;
    @NotNull(message = "Last Name cannot be null")
    private String prenom;

    @NotNull(message = "Phone cannot be null")

    //@Pattern(regexp="(^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$\n)")
    @ValidPhoneNumber(message="Please enter a valid phone number")
    private String telephone;

    @NotNull(message = "Adresse cannot be null")
    private String adresse;

    @NotNull(message = "Email cannot be null")

    @Email(message = "Email should be valid")
    private String email;
}
