package com.learning.carrent.repository;

import com.learning.carrent.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationRepository extends JpaRepository <Location, Long> {

    List<Location> findAllByVoitureId(long voitureId);
}
