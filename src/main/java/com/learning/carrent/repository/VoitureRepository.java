package com.learning.carrent.repository;

import com.learning.carrent.model.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoitureRepository extends JpaRepository <Voiture, Long> {
}
