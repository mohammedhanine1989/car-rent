package com.learning.carrent.mapping;
import com.learning.carrent.dto.LocationDTO;
import com.learning.carrent.model.Location;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface LocationMapper {


    @Mapping(source = "client.id",target = "clientId")
    @Mapping(source = "voiture.id",target = "voitureId")
    LocationDTO locationTolocationDTO(Location location);

    Location locationDTOTolocation(LocationDTO locationDTO);

}
