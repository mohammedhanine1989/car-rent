package com.learning.carrent.mapping;

import com.learning.carrent.dto.ClientDTO;
import com.learning.carrent.model.Client;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDTO clientToClientDTO(Client client);
    Client clientDTOToClient(ClientDTO clientDTO);

}
