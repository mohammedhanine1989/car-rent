package com.learning.carrent.mapping;

import com.learning.carrent.dto.VoitureDTO;
import com.learning.carrent.model.Voiture;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VoitureMapper {
    Voiture voitureDtoToVoiture(VoitureDTO voitureDTO);

    VoitureDTO voitureToVoitureDto(Voiture voiture);
}
