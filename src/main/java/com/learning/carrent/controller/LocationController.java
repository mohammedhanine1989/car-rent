package com.learning.carrent.controller;

import com.learning.carrent.dto.LocationDTO;
import com.learning.carrent.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/locations")
public class LocationController {

    private LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService)
    {
        this.locationService = locationService;
    }

    @GetMapping("")
    public List<LocationDTO> getClients() {

        return locationService.getAllLocation();
    }

    @GetMapping("/{id}")
    public LocationDTO getClients(@PathVariable long id)
    {
        return locationService.getLocationById(id);
    }

    @PostMapping("")
    public ResponseEntity<LocationDTO> insertLocation(@RequestBody LocationDTO locationDTO) {
        return ResponseEntity.ok().body(locationService.insertLocation(locationDTO));
    }
}
