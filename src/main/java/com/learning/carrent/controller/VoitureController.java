package com.learning.carrent.controller;

import com.learning.carrent.dto.VoitureDTO;
import com.learning.carrent.service.VoitureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/voitures")
public class VoitureController {

    private VoitureService voitureService;

    @Autowired
    public VoitureController(VoitureService voitureService) {
        this.voitureService = voitureService;
    }

    @GetMapping("")
    public List<VoitureDTO> getVoitures() {
        return voitureService.getAllVoitures();
    }

    @GetMapping("/api/disponibles")
    public List<VoitureDTO> getVoituresdisponible() {
        return voitureService.getVoituresDisponibles();
    }


    @GetMapping("/{id}")
    public VoitureDTO getVoitures(@PathVariable long id) {
        return voitureService.getVoiture(id);
    }


    @PostMapping("")
    public ResponseEntity<VoitureDTO> insertVoiture(@RequestBody VoitureDTO voitureDTO) {
        return ResponseEntity.ok().body(voitureService.insertVoiture(voitureDTO));
    }

}
