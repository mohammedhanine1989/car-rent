package com.learning.carrent.controller;

import com.learning.carrent.dto.ClientDTO;
import com.learning.carrent.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/clients")
public class ClientController {
    private ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }


    @GetMapping("")
    public List<ClientDTO> getClients() {
        return clientService.getAllClients();
    }


    @GetMapping("/{id}")
    public ClientDTO getClients(@PathVariable long id) {
        return clientService.getClientById(id);
    }

    @PostMapping("")
    public ResponseEntity<ClientDTO> insertClient(@RequestBody @Valid ClientDTO clientDTO) {
        return ResponseEntity.ok().body(clientService.insertClient(clientDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientDTO> upDateClient(@PathVariable long id, @RequestBody @Valid ClientDTO clientDTO) {
        return ResponseEntity.ok().body(clientService.upDateClient(id, clientDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ClientDTO> deleteClient(@PathVariable long id) {
        clientService.deleteByIp(id);
        return ResponseEntity.ok().build();
    }

}
