package com.learning.carrent.validation.validator;

import com.learning.carrent.validation.annotation.ValidPhoneNumber;
import org.springframework.beans.factory.annotation.Configurable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Configurable
public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {


    @Override
    public void initialize(ValidPhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        // The Lookup API requires your phone number in E.164 format
        // E.164 formatted phone numbers must not have spaces in them
        value = value.replaceAll("[\\s()-]", "");

        if ("".equals(value)){
            return false;
        }

       return true;
    }
}